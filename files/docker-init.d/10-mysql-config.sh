#!/usr/bin/env bash

set -Eeuo pipefail

if [[ -z "$POSTFIX_MYSQL_HOST" || -z "$POSTFIX_MYSQL_USER" \
  || -z "$POSTFIX_MYSQL_PASS" || -z "$POSTFIX_MYSQL_DB" ]]; then
    echo "Missing or incomplete MySQL configuration." >/dev/stderr
    exit 1
fi

CFG_DIR="/etc/postfix/mysql/"

find "$CFG_DIR" -type f -name "*.cf" | while read -r CFG_FILE; do
  sed -i "/^user[[:space:]]*=/c\\user = $POSTFIX_MYSQL_USER" -- "$CFG_FILE"
  sed -i "/^password[[:space:]]*=/c\\password = $POSTFIX_MYSQL_PASS" -- "$CFG_FILE"
  sed -i "/^hosts[[:space:]]*=/c\\hosts = $POSTFIX_MYSQL_HOST" -- "$CFG_FILE"
  sed -i "/^dbname[[:space:]]*=/c\\dbname = $POSTFIX_MYSQL_DB" -- "$CFG_FILE"
done

#!/usr/bin/env bash

set -Eeuo pipefail

if [[ -z "$POSTFIX_DEFAULT_TRANSPORT" ]]; then
  exit 0
fi

if ! [[ "$POSTFIX_DEFAULT_TRANSPORT" =~ ^\[.*\]:[[:digit:]]+$ ]]; then
  echo "The default_transport address does not use the square brackets notation [server]:port." \
    "That results in MX lookups on the server address. Are you sure you want that?"
fi

update-postfix-config.sh <(echo "default_transport = smtp:$POSTFIX_DEFAULT_TRANSPORT")

# make the TLS encryption mandatory
postconf -P "smtp/unix/smtp_tls_security_level=encrypt"
# enable XFORWARD
postconf -P "smtp/unix/smtp_send_xforward_command=yes"
# in case there's a next-hop transport, reset the concurrency limits
postconf -P "smtp/unix/smtp_destination_concurrency_limit=\$default_destination_concurrency_limit"
postconf -P "smtp/unix/smtp_destination_rate_delay=\$default_destination_rate_delay"
postconf -P "smtp/unix/smtp_extra_recipient_limit=\$default_extra_recipient_limit"

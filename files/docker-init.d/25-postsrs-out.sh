#!/usr/bin/env bash

set -Eeuo pipefail

if [[ -z "$POSTFIX_POSTSRS_ADDRESS" ]]; then
  exit 0
fi

CAN_MAPS="$(postconf -h sender_canonical_maps)"
update-postfix-config.sh <(echo "sender_canonical_maps = $CAN_MAPS socketmap:inet:$POSTFIX_POSTSRS_ADDRESS:forward")

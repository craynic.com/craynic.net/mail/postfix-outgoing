#!/usr/bin/env bash

set -Eeuo pipefail

if [[ -z "$POSTFIX_OPENDKIM_ADDRESS" ]]; then
  echo "No OpenDKIM address provided, cannot start without DKIM signing." >/dev/stderr
  exit 1
fi

if ! [[ "$POSTFIX_OPENDKIM_ADDRESS" =~ ^\[.*\]:[[:digit:]]+$ ]]; then
  echo "The OpenDKIM address does not use the square brackets notation [server]:port." \
    "That results in MX lookups on the server address. Are you sure you want that?"
fi

update-postfix-config.sh <(echo "smtpd_milters = inet:$POSTFIX_OPENDKIM_ADDRESS")
